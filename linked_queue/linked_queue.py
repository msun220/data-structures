class LinkedQueueNode:
    def __init__(self, value):
        self.value = value
        self.link = None


class LinkedQueue:
    def __init__(self):
        self.head = None
        self.tail = None

    def enqueue(self, value):
        if self.head is None:
            self.head = LinkedQueueNode(value)
            self.tail = self.head
        else:
            self.tail.link = LinkedQueueNode(value)
            self.tail = self.tail.link

    def dequeue(self):
        if self.head is None:
            raise Exception
        else:
            value = self.head.value
            self.head = self.head.link
            if self.head is None:
                self.tail = None
            return value
