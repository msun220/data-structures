class CircularQueue:
    def __init__(self, size):
        self.head = 0
        self.tail = 0
        self.size = size
        self.length = 0
        self.buffer = [None]*size

    def enqueue(self, value):
        if self.length == self.size:
            raise BufferError("Queue is full")

        self.buffer[self.tail] = value
        self.length += 1
        self.tail = (self.tail+1) % self.size

    def dequeue(self):
        if self.length == 0:
            raise IndexError("Queue is empty")

        value = self.buffer[self.head]
        self.length -= 1
        self.head = (self.head+1) % self.size
        return value
