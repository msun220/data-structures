class LinkedListNode:
    def __init__(self, value):
        self.value = value
        self.link = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self._length = 0

    @property
    def length(self):
        return self._length

    def __str__(self):
        return f"LinkedList has length of {self.length}"

    def traverse(self, index):
        if index >= self._length:
            raise IndexError("Out of index range")

        curr_node = self.head
        for i in range(index):
            curr_node = curr_node.link
        return curr_node

    def get(self, index):
        if index >= self._length:
            raise IndexError("Out of index range")
        if index < 0:
            index = self._length + index
        node = self.traverse(index)
        return node.value

    def insert(self, value, index=None):
        node = LinkedListNode(value)

        if (self.head is None) and (self.tail is None):
            self.head = node
            self.tail = node

        elif index == None:
            self.tail.link = node
            self.tail = self.tail.link

        else:
            if index > self._length:
                raise IndexError("Out of index range")

            if index < 0:
                index = self._length + index + 1

            if index == 0:
                node.link = self.head
                self.head = node

            else:
                prev_node = self.traverse(index-1)

                if index <= (self._length-1):
                    node.link = prev_node.link
                    prev_node.link = node
                else: #if index == (self._length) which means index is last+1
                    self.tail.link = node
                    self.tail = self.tail.link

        self._length += 1

    def remove(self, index):
        if index >= self._length:
            raise IndexError("Out of index range")
        if index < 0:
            index = index + self._length
        if index == 0:
            value = self.head.value
            self.head = self.head.link
        elif index <= self._length:
            prev_node = self.traverse(index-1)
            curr_node = self.traverse(index)
            value = curr_node.value
            if curr_node == self.tail:
                prev_node.link = None
                self.tail = prev_node
            else:
                prev_node.link = curr_node.link

        self._length -= 1
        return value
